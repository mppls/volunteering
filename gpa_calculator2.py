def get_letter_grades():
    list = []
    grade1 = raw_input("Enter a grade: ")
    grade2 = raw_input("Enter a grade: ")
    grade3 = raw_input("Enter a grade: ")
    grade4 = raw_input("Enter a grade: ")
    list.append(grade1)
    list.append(grade2)
    list.append(grade3)
    list.append(grade4)
    return list

def letters_to_points(list):
    points = 0
    for x in list:    
        x = x.upper()
        if   (x == 'A'):
            points += 4.0
        elif (x == 'B'):
            points += 3.0
        elif (x == 'C'):
            points += 2.0
        elif (x == 'D'):
            points += 1.0
        elif (x == 'F'):
            points += 0.0
    return points

def get_gpa(points):
    return points / len(list)

name    = raw_input("Enter your name:  ")
list    = get_letter_grades()
points  = letters_to_points(list)
gpa     = get_gpa(points)
print "The GPA for", name, "is", gpa
