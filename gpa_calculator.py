def letter_to_points(x):
    x = x.upper()
    if (x == 'A'):
        return 4.0
    elif (x == 'B'):
        return 3.0
    elif (x == 'C'):
        return 2.0
    elif (x == 'D'):
        return 1.0
    elif (x == 'F'):
        return 0.0
    
def get_grade_points(list):
    points = 0
    for letter in list:
        points += letter_to_points(letter)
    return points

def get_gpa(points):
    return points / len(list)

list = []
name = 'Micah'
grade1 = 'B'
grade2 = 'A'
grade3 = 'B'
grade4 = 'A'
list.append(grade1)
list.append(grade2)
list.append(grade3)
list.append(grade4)
points = get_grade_points(list)
gpa = get_gpa(points)
print "The GPA for", name, "is", gpa
