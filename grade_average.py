list  = []
test1 = 80
test2 = 65
test3 = 70
test4 = 85

def get_average():
    total = 0
    for num in list:
        total += num
    return total / len(list)

def add_to_list(grade):
    list.append(grade)

def determine_letter_grade(grade):
    if 90 <= grade <= 100:
        return 'A'
    elif 80 <= grade <= 89:
        return 'B'
    elif 70 <= grade <= 79:
        return 'C'
    elif 60 <= grade <= 69:
        return 'D'
    else:
        return 'F'
        
add_to_list(test1)
add_to_list(test2)
add_to_list(test3)
add_to_list(test4)
average = get_average()
print("A " + str(average) + " is a " + str(determine_letter_grade(average)))
